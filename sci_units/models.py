from numbers import Number

DIV_RELATION = '/'

class UnitType(type):
  registered_units = {} # TODO extract registration logic to e.g. `UnitRegister` class

  class UnitNotFound(Exception):
    def __init__(self, compound_unit_identifier):
      super().__init__(f'Unit "{compound_unit_identifier}" was not found.')

  # factory methods
  def __ror__(cls, other):
    if isinstance(other, Number):
      return cls(value = other)
    return NotImplemented

  # lookups methods for compound units
  def __truediv__(cls, other):
    compound_unit_identifier = cls.create_compound_unit_identifier(
      first_unit = cls, relation = DIV_RELATION, second_unit = other)

    try:
      return cls.registered_units[compound_unit_identifier]
    except KeyError:
      raise cls.UnitNotFound(compound_unit_identifier)

  @staticmethod
  def create_compound_unit_identifier(first_unit, relation, second_unit):
    return first_unit.get_full_name(), relation, second_unit.get_full_name()

  def is_compound_unit(cls):
    return issubclass(cls, CompoundUnit)

  def get_full_name(cls):
    return f'{cls.__module__}.{cls.__name__}'

  def get_unit_identifier(cls):
    if cls.is_compound_unit():
      return cls.create_compound_unit_identifier(first_unit  = cls.first_unit,
                                                 relation    = cls.relation,
                                                 second_unit = cls.second_unit)
    return cls.get_full_name()

class UnitBase(metaclass = UnitType):
  value = 0

  # TODO consider to move this code into `UnitType.__new__` method,
  # because we are not able to use `cls is Unit` in `__init_subclass__` so we need use
  # `base_unit_type` kwarg, which is a little bit ugly.
  @classmethod
  def __init_subclass__(cls, *, base_unit_type = False, **kwargs):
    super().__init_subclass__(**kwargs)
    if not base_unit_type:
      cls.registered_units[cls.get_unit_identifier()] = cls

  def __init__(self, value): self.value = value
  def __str__(self):         return f'{self.value}{type(self).__name__}'
  def __repr__(self):        return f'{type(self).__name__}(value = {self.value})'

  def __eq__(self, other):
    if type(self) is type(other) and self.value == other.value:
      return True
    return NotImplemented

class Unit(UnitBase, base_unit_type = True): pass

class CompoundUnit(UnitBase, base_unit_type = True):
  @classmethod
  def __init_subclass__(cls, *, first_unit, second_unit, relation, **kwargs):
    assert first_unit and second_unit and relation, \
      "First unit nor second unit nor relation must not be None."

    cls.first_unit  = first_unit
    cls.second_unit = second_unit
    cls.relation    = relation

    super().__init_subclass__(**kwargs)

class m(Unit):       pass
class s(Unit):       pass
class m_per_s(CompoundUnit, first_unit = m, relation = DIV_RELATION, second_unit = s): pass

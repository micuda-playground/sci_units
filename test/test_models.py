import pytest
from operator         import eq, ne
from sci_units.models import DIV_RELATION, UnitType, m, s, m_per_s

class BaseUsageTests:
  def test_simple_units(self):
    actual = 9|m

    assert isinstance(actual, m)
    assert actual.value == 9

  def test_compound_units(self):
    actual = 9|m/s

    assert isinstance(actual, m_per_s)
    assert actual.value == 9

operator_repr_map = {
  eq: '==',
  ne: '!='
}

def get_test_equality_id(param):
  if param in [eq, ne]:
    return operator_repr_map[param]
  return str(param)

@pytest.mark.parametrize(
  ' actual,  operator,  expected',
  [(   9|m,        eq,       9|m),
   (   9|m,        ne,      18|m),
   (   9|m,        ne,       9|s)],
  ids = get_test_equality_id)
def test_equality(actual, operator, expected):
  assert operator(actual, expected)

def test_truediv_lookup():
  assert m/s is m_per_s

  actual_compound_unit_identifier = UnitType.create_compound_unit_identifier(
    first_unit = s, relation = DIV_RELATION, second_unit = m)

  with pytest.raises(UnitType.UnitNotFound,
                     match=r'{}'.format(str(actual_compound_unit_identifier))):
    assert s/m
